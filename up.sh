#!/bin/bash

# Up -- CentOS/RHEL Update Tool (Version 1.0)
# Advanced command to fully update system: "up" Adding the option "--clean" will
# remove cached data and select the fastest mirrors.
#Modified this script to work with rpm distros
#The original script was made by Joe Collins, see here: https://github.com/EzeeLinux/up-debian_ubuntu_update_tool
#Support the Ezee Linux project www.ezeelinux.com (GNU/General Public License version 2.0)

# Set BASH to quit script and exit on errors:

set -e

# Functions:

update() {

echo "Starting full system update..."
sudo yum update -y
sudo yum upgrade -y

}

clean() {

echo "Cleaning up..."
sudo yum clean all -y 
sudo rm -r -f /var/cache/yum

}

leave() {

echo "--------------------"
echo "- Update Complete! -"
echo "--------------------"
exit

}

up-help() {

cat << _EOF_

This derivative of the Up tool is meant to automate the update procedure for RHEL and CentOS systems.

 Commands:
    up = full system update.

    Running "up" with no options will update the system (yum update & yum upgrade)
    
    up --clean = runs the yum clean all command

    Adding the "--clean" option will invoke the yum clean all command which removes cached data.

    up --help = shows this help page.

_EOF_

}

# Execution.

# Tell 'em who we are...

echo "Up -- CentOS/RHEL Update Tool (Version 1.0)"

# Update and clean:

if [ "$1" == "--clean" ]; then
    update
    clean
    leave
fi

if [ "$1" == "--help" ]; then
    up-help
    exit
fi

# Check for invalid argument

if  [ -n "$1"  ]; then
    echo "Up Error: Invalid argument. Try 'up --help' for more info."
    exit 1
fi

update
leave
