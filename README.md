This is a modified version of the up-debian_ubuntu_update_tool created by Joe Collins to work on rpm based distros.

The original script can be found here: https://github.com/EzeeLinux/up-debian_ubuntu_update_tool

Support the Ezee Linux project if you feel so inclined: http://www.ezeelinux.com/

The usage has been detailed below:

Running "up" with no options will run a yum update and a yum upgrade

up --clean = cleans up the cached data.

Adding the "--clean" option will invoke the yum clean all command to remove cached data.

up --help = shows this help page.

Note: be sure to run the chmod 700 up command to make the file usable.